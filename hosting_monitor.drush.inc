<?php

/**
 * Implements drush_HOOK_pre_COMMAND().
 *
 * Map values from node into command line arguments.
 */
function drush_hosting_monitor_pre_hosting_task($task) {
  $task = &drush_get_context('HOSTING_TASK');

  if ($task->task_type == 'monitor') {
    // Pass the NID as an argument to the provision-monitor command.
    $task->args[1] = $task->ref->nid;
  }

}
