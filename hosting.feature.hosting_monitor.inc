<?php
/**
 * @file
 *   The hosting feature definition for the Aegir Monitoring API.
 */

/**
 * Register a hosting feature with Aegir.
 *
 * @return
 *  associative array indexed by feature key.
 */
function hosting_monitor_hosting_feature() {
  $features['probes'] = array(
    'title' => t('Aegir Monitoring API'),
    'description' => t('Provides storage and an API for probing Aegir entities.'),
    'status' => HOSTING_FEATURE_DISABLED,
    'module' => 'hosting_monitor',
    'group' => 'experimental',
  );
  return $features;
}
