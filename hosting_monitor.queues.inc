<?php

/**
 * Implements hook_hosting_queues().
 *
 * Returns a list of queues that this module needs to manage.
 */
function hosting_monitor_hosting_queues() {
  $enabled_types = hosting_monitor_get_enabled_probe_types();
  $queues = array();
  foreach ($enabled_types as $type) {
    $singular = t($type);
    $plural = t($type . 's');
    $queues["monitor_{$type}"] = array(
      'type' => 'batch',
      'name' => t('!type monitoring', array('!type' => ucwords($singular))),
      'description' => t('Monitor !type.', array('!type' => ucwords($plural))),
      'frequency' => strtotime("1 min", 0),
      'min_threads' => 6,
      'max_threads' => 12,
      'threshold' => 100,
      'total_items' => hosting_monitor_get_queue_info($type, 'total_items'),
      'singular' => $singular,
      'plural' => $plural,
    );
  }
  return $queues;
}

function hosting_monitor_get_queue_info($type, $key) {
  $queue_info = array(
    'site' => array(
      'total_items' => hosting_site_count(),
    ),
    'server' => array(
      'total_items' => hosting_monitor_count_servers(),
    ),
  );
  return $queue_info[$type][$key];
}

/**
 * The main queue callback for the 'probes_site' queue.
 *
 * Function name constructed as hosting_QUEUE_NAME_queue().
 *
 * @see hosting_run_queue()
 */
function hosting_monitor_site_queue($count) {
  $sites = hosting_monitor_get_sites();
  hosting_monitor_dispatch_probes('site', $sites);
}

/**
 * Get sites that need to be probed.
 */
function hosting_monitor_get_sites() {
  $nids = db_query("SELECT nid FROM {hosting_site} WHERE status = :status", array(':status' => HOSTING_SITE_ENABLED))->fetchCol();
  $sites = node_load_multiple($nids);
  return $sites;
}

/**
 * Returns a count of servers.
 *
 * This is used by cron and statistics to batch the number of servers that are
 * processed with each call.
 *
 * @return
 *   The number of servers in the Aegir deployment.
 */
function hosting_monitor_count_servers() {
  $query = db_select('hosting_server')
    ->addTag('hosting_server_count')
    ->fields('hosting_server', array('nid'));
  return $query->countQuery()->execute()->fetchField();
}

/**
 * The main queue callback for the 'probes_server' queue.
 *
 * Function name constructed as hosting_QUEUE_NAME_queue().
 *
 * @see hosting_run_queue()
 */
function hosting_monitor_server_queue($count) {
  $servers = hosting_monitor_get_servers();
  hosting_monitor_dispatch_probes('server', $servers);
}

/**
 * Get servers that need to be probed.
 */
function hosting_monitor_get_servers() {
  $servers = node_load_multiple(array(), array('type'=>'server'));
  return $servers;
}

