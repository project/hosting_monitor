<?php
/**
 * @file Hook documentation
 */

/**
 * Return a list of probe classes.
 */
function hook_hosting_monitor_probes() { 
  return array(
    'UpdateStatusProbe',
  );
}

/**
 * Alter a list of probe classes.
 */
function hook_hosting_monitor_probes_alter(&$probes) {
  $probes['user_count'] = 'UserCountProbe';
}

/**
 * Implements hook_form_alter().
 *
 * This is an example of how one might alter the probe settings forms. Since
 * the forms are built using an additional parameter ($type) passed in from
 * hook_menu(), the resulting forms differ.
 */
function hosting_monitor_example_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == 'hosting_monitor_form') {
    $type = $form_state['build_info']['args'][0];
    if ($type) {
      // Alter all probe settings forms, regardless of type.
    }
    else {
      // Alter the general probe settings form.
    }
    switch ($type) {
      case 'site':
        // Only alter the site probes settings form.
        break;
      case 'server':
        // Only alter the server probes settings form.
        break;
    }
  }
}

