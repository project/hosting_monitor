<?php

/**
 * @file The HostingProbe class.
 */

class HostingProbe {

  //const DAILY = 24 * 60 * 60; // PHP 5.6+
  const DAILY = 86400;

  protected $name = FALSE;

  protected $type = FALSE;

  protected $probe = FALSE;

  protected $context = FALSE;

  protected $label = FALSE;

  protected $description = FALSE;

  protected $enabled = FALSE;

  protected $frequency = self::DAILY;

  public function __construct($name = FALSE) {
    if ($name && !$this->name) {
      $this->name = $name;
    }
  }

  public function getProbe() {
    return $this->probe;
  } 

  public function getContext() {
    return $this->context;
  } 

  public function getType() {
    return $this->type;
  } 

  public function getName() {
    return $this->name;
  } 

  public function getLabel() {
    return $this->label;
  }

  public function getDescription() {
    return $this->description;
  } 

  public function isEnabled() {
    $name = $this->getName();
    $enabled = variable_get("hosting_monitor_{$name}_enabled", NULL);
    if (!is_null($enabled)) {
      return $enabled;
    }
    return $this->enabled;
  }

  public function setEnabled($enabled) {
    $name = $this->getName();
    variable_set("hosting_monitor_{$name}_enabled", $enabled);
  }

  public function getFrequency() {
    $name = $this->getName();
    $frequency = variable_get("hosting_monitor_{$name}_frequency", NULL);
    if (!is_null($frequency)) {
      return $frequency;
    }
    return $this->frequency;
  }

  public function setFrequency($frequency) {
    $name = $this->getName();
    variable_set("hosting_monitor_{$name}_frequency", $frequency);
  }

}

trait DrushShellExecDispatcher {
  public function dispatch($alias) {
    $command = $this->getCommand($alias);
    drush_log(dt('Dispatching probe: `:command`', array(':command' => $command)));
    if ($result = drush_shell_exec($command)) {
      $output = drush_shell_exec_output();
      // Allow the probe to call a function to parse, or otherwise react to
      // the probe. For example, while we only keep the latest probe
      // result, time-series data could be recorded separately from within
      // this callback.
      return $this->parse($output);
    }
  }
}

trait DrushEvalCommand {
  /**
   * Callback to format a Drush PHP function call probe.
   */
  function getCommand($alias) {
    // TODO: Investigate the security implications of allowing this.
    return drush_find_drush() . ' ' . $alias . ' php-eval "' . $this->probe . '"';
  }

}

trait DrushSystemCommand {
  /**
   * Callback to format a Drush system call probe.
   */
  function getCommand($alias) {
    // TODO: Investigate the security implications of allowing this.
    return drush_find_drush() . ' ' . $alias . ' php-eval "system(' . $this->probe . ')"';
  }

}

trait DrushQueryCommand {
  /**
   * Callback to format a Drush site database query probe.
   */
  function getCommand($alias) {
    // TODO: Investigate the security implications of allowing this.
    return drush_find_drush() . ' ' . $alias . ' sql-query "' . $this->probe . '"';
  }
}

trait SingleResultParser {
  /**
   * Callback to parse a single result from probe results.
   */
  function parse($result) {
    return $result[0];
  }
}
