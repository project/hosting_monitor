<?php

class ResultSet {

  protected $results;

  public function __construct(array $results = array()) {
    $this->results = $results;
  }

  public function getResults() {
    return $this->results;
  }

  public function getResult($name) {
    return $this->results[$name];
  }

  /**
   * Record probe data.
   */
  function recordResults($type, $nid) {
    $data = serialize($this->getResults());
    $entry_exists = db_query("SELECT nid FROM {hosting_monitor} WHERE nid = :nid and type = :type", array(':nid' => $nid, ':type' => $type))->fetchField();
    if (FALSE === $entry_exists) {
      db_insert('hosting_monitor')
      ->fields(array(
        'type' => $type,
        'nid' => $nid,
        'last_probed' => time(),
        'data' => $data,
      ))
      ->execute();
    }
    else {
      db_update('hosting_monitor')
        ->fields(array(
          'last_probed' => time(),
          'data' => $data,
        ))
        ->condition('type', $type)
        ->condition('nid', $nid)
        ->execute();
    }
  }

  /**
   * Fetch a set of results from the database.
   */
  function fetchResults($type, $nid) {
    $results = db_query("SELECT data FROM {hosting_monitor} WHERE nid = :nid and type = :type", array(':nid' => $nid, ':type' => $type))->fetchField();
    return unserialize($results);
  }
  
  /**
   * Fetch a single result.
   */
  function fetchResult($type, $nid, $name) {
    $results = $this->fetchResults($type, $nid);
    if (is_array($results) && array_key_exists($name, $results)) {
      return $results[$name];
    }
    else {
      return FALSE;
    }
  }

}

