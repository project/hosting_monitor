<?php

/**
 * Settings form callback.
 */
function hosting_monitor_form($form, &$form_state, $type) {
  if ($type) {
    drupal_add_css(drupal_get_path('module', 'hosting') . '/hosting.css');
    drupal_add_css(drupal_get_path('module', 'hosting_monitor') . '/css/form.css');
    $queues = hosting_get_queues();
    $queue = $queues["monitor_{$type}"];
    $probes = hosting_monitor_get_probes($type);

    // Add some (alterable) definitions for the table columns.
    // @see theme_hosting_monitor_form().
    $form['#table_build_info'] = array(
      'enabled' => '',
      'label' => t('Label'),
      'description' => t('Description'),
      'frequency' => array(
        'data' => t('Frequency'),
        'class' => array('hosting-queue-frequency-head'),
      ),
    );

    $form['#tree'] = TRUE;
    foreach ($probes as $name => $probe) {
      $form[$name]['enabled'] = array(
        '#type' => 'checkbox',
        '#default_value' => $probe->isEnabled(),
      );
      $form[$name]['label'] = array(
        '#type' => 'item',
        '#value' => $name,
        '#description' => $probe->getLabel(),
      );
      $form[$name]['description'] = array(
        '#type' => 'item',
        '#value' => $name,
        '#description' => $probe->getDescription(),
      );
      $form[$name]['frequency']['#prefix'] = "<div class='hosting-queue-frequency'>";
      $form[$name]['frequency']['#suffix'] = '</div>';
      $form[$name]['frequency']['items'] = array(
        '#markup' => t('Every'),
        '#prefix' => "<div class='hosting-queue-frequency-items'>",
        '#suffix' => "</div>",
      );
      $form[$name]['frequency']['ticks'] = array(
        '#type' => 'textfield',
        '#default_value' => hosting_monitor_get_frequency_info($probe->getFrequency(), 'ticks'),
        '#maxlength' => 5,
        '#size' => 5,
        '#attributes' => array(
          'class' => array('hosting-select-frequency-ticks'),
        ),
      );
      $form[$name]['frequency']['unit'] = array(
        '#type' => 'select',
        '#options' => hosting_monitor_get_units(),
        '#default_value' => hosting_monitor_get_frequency_info($probe->getFrequency(), 'length'),
        '#attributes' => array(
          'class' => array('hosting-select-frequency-unit'),
        ),
      );
      $form['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Save changes'),
      );
    }
    return $form;
  }
  else {
    $form['hosting_monitor_enabled_types'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Enabled probe types'),
      '#options' => hosting_monitor_get_probe_types(),
      '#default_value' => hosting_monitor_get_enabled_probe_types(),
      '#description' => t('The enabled status of the installed probe types.'),
    );
    return system_settings_form($form);
  }
}

/**
 * Helper function to return frequency calculations.
 */
function hosting_monitor_get_frequency_info($frequency, $info) {
  $units = hosting_monitor_get_units();
  $return = array();
  foreach (array_reverse(array_keys($units)) as $length) {
    $unit = $units[$length];

    if (!($frequency % $length)) {
      $return['ticks'] = $frequency / $length;
      $return['length'] = $length;
      break;
    }
  }
  return $return[$info];
}

/**
 * Helper function to return time units.
 */
function hosting_monitor_get_units() {
  $units = array(
    strtotime("1 minute", 0) => t("Minutes"),
    strtotime("1 hour", 0) => t("Hours"),
    strtotime("1 day", 0) => t("Days"),
    strtotime("1 week", 0) => t("Weeks"),
    strtotime("1 month", 0) => t("Months"),
  );
  return $units;
}

/**
 * Validation callback for the hosting probes form.
 */
function hosting_monitor_form_validate($form, &$form_state) {
  foreach (hosting_monitor_get_probes() as $name => $probe) {
    if (isset($form_state['values'][$name])) {
      if ($form_state['values'][$name]['frequency']['ticks'] && !is_numeric($form_state['values'][$name]['frequency']['ticks'])) {
        form_set_error($name, t('Please enter a valid frequency.'));
      }
    }
  }
}

/**
 * Submit callback for the hosting probes form.
 */
function hosting_monitor_form_submit($form, &$form_state) {
  foreach (hosting_monitor_get_probes() as $name => $probe) {
    if (isset($form_state['values'][$name])) {
      $probe->setEnabled($form_state['values'][$name]['enabled']);
      $frequency = $form_state['values'][$name]['frequency']['ticks'] * $form_state['values'][$name]['frequency']['unit'];
      $probe->setFrequency($frequency);
    }
  }
  drupal_set_message(t('The queue settings have been updated.'));
}

/**
 * Submit handler for the general settings form.
 */
function hosting_monitor_general_settings_submit($form, &$form_state) {
  // Enable/disable the probe type forms.
  menu_rebuild();
}

/**
 * Implements hook_form_alter().
 */
function hosting_monitor_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == 'hosting_monitor_form') {
    $type = $form_state['build_info']['args'][0];
    if (!$type) {  // Alter the general probe settings form.
      // Our submit handler has to come last, since it depends on the form
      // settings being saved.
      $form['#submit'][] = 'hosting_monitor_general_settings_submit';
    }
  }
}

/**
 * Theme function to render the queue configuration form.
 */
function theme_hosting_monitor_form($variables) {
  $form = $variables['form'];
  $probes = hosting_monitor_get_probes();

  $rows = array();
  // Pick up the header/column definitions from the form.
  // @see hosting_monitor_form().
  $header = $columns = $form['#table_build_info'];
  foreach ($probes as $name => $probe) {
    $row = array();
    foreach ($columns as $column => $info) {
      $row[] = drupal_render($form[$name][$column]);
    }
    $rows[] = $row;
  }
  $output = theme('table', array('header' => $header, 'rows' => $rows));
  $output .= drupal_render($form['submit']);
  $output .= drupal_render_children($form);
  return $output;
}

