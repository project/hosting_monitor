<?php

/**
 * Implements hook_drush_command().
 */
function provision_monitor_drush_command() {
  $items = array();
  $items['provision-monitor'] = array(
    'arguments' => array(
      'nid' => dt('The ID of the node to probe.'),
    ),
    'required-arguments' => TRUE,
    'description' => dt('Run probes.'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
    'allow-additional-options' => TRUE,
  );
  return $items;
}

/**
 * Command callback for the 'provision-monitor' command.
 */
function drush_provision_monitor($nid) {
  $type = d()->type;
  // We just process the relevant queue, passing in the node ID, and forcing
  // execution.
  provision_backend_invoke('@hostmaster', "hosting-monitor_{$type}", array($nid), array('--force'));
}
