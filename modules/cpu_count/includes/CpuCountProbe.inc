<?php

class CpuCountProbe extends HostingProbe {

  use DrushShellExecDispatcher, DrushSystemCommand, SingleResultParser;

  protected $name = 'cpu_count';

  protected $type = 'server';

  protected $enabled = TRUE;

  protected $probe = 'nproc';

  protected $label = 'CPU count';

  protected $description = 'Count of CPUs on the server';
}

